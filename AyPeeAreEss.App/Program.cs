﻿// See https://aka.ms/new-console-template for more information

using System.Net.Sockets;
using System.Text;
using AyPeeAreEss.Kiss;
using Decoder = AyPeeAreEss.Ax25.Decoder;

var socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
socket.Connect("localhost", 8001);

var client = new Client(socket);


while (true)
{
    var ax25Frame = client.GetPacket();
    
    var decodedAx25 = Decoder.Decode(ax25Frame);
    
    if (decodedAx25 is not null) Console.WriteLine(decodedAx25);
}
