﻿using System.Text;

namespace AyPeeAreEss.Ax25;

public static class Decoder
{
     private const int CallsignLength = 7;
     public static Frame Decode(byte[] bytes)
     {
          var destinationSpan = bytes.AsSpan(0, CallsignLength);
          var sourceSpan = bytes.AsSpan(CallsignLength, CallsignLength);
          
          var sourceIsLastAddress = _isLastAddress(sourceSpan);
          
          var destination = _decodeAddress(destinationSpan);
          var source = _decodeAddress(sourceSpan);
          
          if (sourceIsLastAddress) return new(
               source, 
               destination, 
               bytes.AsSpan(CallsignLength * 2, bytes.Length - CallsignLength * 2).ToArray(), 
               FrameType.Information
          );
          
          var currentRepeaterBytes = bytes.AsSpan(CallsignLength * 2, CallsignLength);
          
          if (_isLastAddress(currentRepeaterBytes))
          {
               return new(
                    source,
                    destination,
                    bytes.AsSpan(CallsignLength * 3, bytes.Length - CallsignLength * 3).ToArray(),
                    FrameType.Information,
                    new[] {_decodeAddress(currentRepeaterBytes)}
               );
          }
          
          
          return new(
               source,
               destination,
               bytes.AsSpan(CallsignLength * 3, bytes.Length - CallsignLength * 3).ToArray(),
               FrameType.Information,
               new []
               {
                    _decodeAddress(currentRepeaterBytes),
                    _decodeAddress(bytes.AsSpan(CallsignLength * 3, CallsignLength))
               }
          );
     }

     private static Host _decodeAddress(Span<byte> bytes)
     {
          for (var i = 0; i < bytes.Length; i++)
          {
               bytes[i] = i == bytes.Length - 1
                    ? (byte) ((bytes[i] & 0b1111) >> 1)
                    : (byte) (bytes[i] >> 1);
          }
     
          return new (
               Encoding.ASCII.GetString(bytes.Slice(0,6)),
               bytes[^1]
          );
     }

     private static bool _isLastAddress(ReadOnlySpan<byte> bytes) => (bytes[^1] & 0b1) == 1;
}