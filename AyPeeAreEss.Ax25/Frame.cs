﻿namespace AyPeeAreEss.Ax25;

public enum FrameType
{
    Information,
    Supervisory,
    Unnumbered
}

public record Frame(Host Source, Host Destination, byte[] Payload, FrameType FrameType, Host[]? Repeaters = null);
    
// 0bXXXXXX00 - I Frame
// 0bXXXXXX10 - S Frame
// 0bXXXXXX11 - U Frame

public record Host(string Callsign, int Ssid);

