﻿namespace AyPeeAreEss.Encoding

open System.Text

module Decoder =
    type Ax25Packet =
        string
    let Decode (pack: byte[]) =
        Encoding.Default.GetString pack