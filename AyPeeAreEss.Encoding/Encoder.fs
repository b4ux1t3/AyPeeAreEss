﻿namespace AyPeeAreEss.Decoder
open System.Text

module Encoder =
    let Encode (str : string) = Encoding.Default.GetBytes str
