﻿using System.Net.Sockets;
using System.Text;

namespace AyPeeAreEss.Kiss;

public class Client
{
    private const byte FEnd = 0xC0;
    private const byte TFEnd = 0xDC;
    private const byte FEscape = 0xDB;
    private const byte TFEscape = 0xDD;

    private readonly Socket _sock;

    private readonly byte[] _buffer = new byte[ushort.MaxValue];
    private readonly List<byte> _currentPacket = new();
    private char[] _charBuffer = new char[ushort.MaxValue];
    
    public Client(Socket sock)
    {
        _sock = sock;
    }

    public byte[] GetPacket()
    {
        var bytes = _readPacket();

        return _parsePacket(bytes);
    }

    private byte[] _readPacket()
    {
        while (true)
        {
            var bytesReceived = _sock.Receive(_buffer);

            if (_buffer[0] != FEnd && _currentPacket.Count < 1) throw new InvalidKissPacketException("We have received an an invalid packet; There is no FEND at the beginning of the packet stream, and we are not in the middle of a large packet.");
            if (_buffer[bytesReceived - 1] != FEnd)
            {
                for (var i = 0; i < bytesReceived; i++)
                {
                    _currentPacket.Add(_buffer[i]);
                }

                continue;
            }

            if (_currentPacket.Count <= 0) return _buffer.AsSpan(0, bytesReceived).ToArray();
            
            for (var i = 0; i < bytesReceived; i++)
            {
                _currentPacket.Add(_buffer[i]);
            }

            return _currentPacket.ToArray();
        }
    }

    private static byte[] _parsePacket(byte[] bytes)
    {
        if (bytes[0] != FEnd) throw new InvalidKissPacketException("Packet did not start with FEND");
        if (bytes[^1] != FEnd) throw new InvalidKissPacketException("Packet did not end with FEND");
        
        return bytes.AsSpan(2, bytes.Length - 2).ToArray();
    }
}