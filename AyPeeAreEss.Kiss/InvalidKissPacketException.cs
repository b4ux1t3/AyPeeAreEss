﻿namespace AyPeeAreEss.Kiss;

public class InvalidKissPacketException : Exception
{
    public InvalidKissPacketException(string message) : base(message)
    {
            
    }
}