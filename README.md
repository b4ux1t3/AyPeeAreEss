# AyPeeAreEss

AyPeeAreEss is a lightweight, .NET-based utility for parsing APRS packets over an arbitrary protocol.

Currently it supports AX.25 over KISS, though further link and network layers are on the horizon.

The main functionality is contained in `AyPeeAreEss.Ax25`.
